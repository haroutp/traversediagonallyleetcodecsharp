﻿using System;

namespace TraverseDiagonallyLeetCode
{
    class Program
    {
        static void Main(string[] args)
        {
            int[][] arr1 = new int[1][];

            arr1[0] = new int[2]{1, 2}; 
            // int[][] arr1 = new int[3][];

            // arr1[0] = new int[3] {1, 2, 3};
            // arr1[1] = new int[3] {4, 5, 6};
            // arr1[2] = new int[3] {7, 8, 9};
            System.Console.WriteLine(arr1.Length + " " + arr1[0].Length);
            //System.Console.WriteLine((arr1 == null) + " " + (arr1.GetLength(0) == 0));
            
            int[] res = FindDiagonalOrder(arr1);
            
            foreach (var item in res)
            {
                System.Console.Write(item + " ");
            }
            
        }
        public static int[] FindDiagonalOrder(int[][] matrix) {
            if(matrix == null || matrix.GetLength(0) == 0){
                return new int[0];
            }

            int i = 0;
            int j = 0;
            int n = matrix.Length;
            int m = matrix[0].Length;
            int[] res = new int[n * m];

            for (int k = 0; k < n*m; k++)
            {
                res[k] = matrix[i][j];
                if(((i + j) % 2) == 0){
                    if(j == m - 1){
                        i++;
                    }else if(i == 0){
                        j++;
                    }else{
                        i--;
                        j++;
                    }
                }else{
                    if(i == n - 1){
                        j++;
                    }else if(j == 0){
                        i++;
                    }else{
                        i++;
                        j--;
                    }
                }
                
                
            }
            return res;
        }
    }
}
